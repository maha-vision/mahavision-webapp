# Mahavison webapp
This is the webapp for the [Mahavision smart glasses](https://gitlab.com/maha-vision/mahavision-arduino).

The webapp uses bluetooth web, see the [support list](https://github.com/WebBluetoothCG/web-bluetooth/blob/master/implementation-status.md) for the supported web browsers

The webapp is hosted at https://maha-vision.gitlab.io/mahavision-webapp/