// document.getElementById("demo").innerHTML = "Drink all the booze. Hack all the things";

// document.getElementById("c1r1").innerText = "COOl"
var SERVER;
const services_json = {
    '4f464e49-0000-1000-8000-00805f9b34fb': {
        'name': "id service",
        'characteristics': {
            '46495253-5430-4e41-4d45-303030303030': {'display_name': 'name', 'rtl': true },
            '4c415354-304e-414d-4530-303030303030': {'display_name': 'family name' , 'rtl': true},
            '64696469-0000-1000-8000-00805f9b34fb': {'display_name': 'id', 'rtl' : false}
        }
    },
    '45415354-4552-3053-4552-564943453030': {
        'name': "easter egg service",
        'characteristics': {
            '45415354-4552-3045-4747-304348415230': {'display_name': 'eggs', 'rtl': true },
        }
    },
    '46524945-4e44-5330-5345-525649434530': {
        'name': "friends service",
        'characteristics': {
            '46524945-4e44-5330-4348-415230303030': {'display_name': 'friend', 'rtl': true },
        }
    },
}


// Connect to the Bluetooth device and open a Server
async function connect_device(name) {
    let services_list = [];
    for (const [key, value] of Object.entries(services_json)) {
        services_list.push(key);
      }

    name = 'mahavision-' + name;

    return navigator.bluetooth.requestDevice({
        optionalServices: services_list,
        filters: [{ name:  name}],
      })
      .then(async device => {
          let server = await device.gatt.connect();
          SERVER = server;
          return server;
        } )
}

// Scan the device and populate the tables
async function first_scan() {

    let mac = document.getElementById("mac");
    if ( mac.value == "" ) {
        alert("יש להזין את מזהה המכשיר כדי לסרוק");
        return;
    }
    let server = await connect_device(mac.value);
    clear_all_tables();
    download_data()
}


function clear_table(table, skip=1) {
    for ( i = 1, row=null; row = table.rows[i]; i++ ) {
        row.remove();
    }

}

// Updates the forms
function clear_all_tables() {
    let friends_table = document.getElementById("friends_table");
    let prizes_table = document.getElementById("prizes_table");

    clear_table(friends_table)
    clear_table(prizes_table)
}


// Download the data and populate the forms
function download_data() {

    if ( SERVER == null ) { // Catch if still haven't scanned
        alert("יש לסרוק לפני כתיבת נתונים למשקפיים");
        return;
    }

    clear_all_tables();

    read_all(SERVER)
    .then(data => {
        console.log(data); // Debug - log all data

        // get user data - name & surname
        var names = data['4f464e49-0000-1000-8000-00805f9b34fb']['characteristics'];
        document.getElementById("name").value = names['46495253-5430-4e41-4d45-303030303030']['value'];
        document.getElementById("surname").value = names['4c415354-304e-414d-4530-303030303030']['value'];

        // Get User friends
        if ('46524945-4e44-5330-5345-525649434530' in data  ) {
            let friends_table = document.getElementById("friends_table");
            let friends = data['46524945-4e44-5330-5345-525649434530']['characteristics']['46524945-4e44-5330-4348-415230303030']['value']
            populate_table(friends, friends_table, 1, splitter='-');

        }
        else { console.log("No Friends") }

    
        // Get prizes
        // Same logic as in friends
        if ('45415354-4552-3053-4552-564943453030' in data  ) {
            let prizes = data['45415354-4552-3053-4552-564943453030']['characteristics']['45415354-4552-3045-4747-304348415230']['value']
            let prizes_table = document.getElementById("prizes_table");
            populate_table(prizes, prizes_table, 1);
        }
        else { console.log("No Prizes") }

        alert("קריאה בוצעה בהצלחה");
    })
}

// This function uploads the data from the forms
async function upload_data() {
    if ( SERVER == null ) { // Catch if still haven't scanned
        alert("יש לסרוק לפני קריאת נתונים מהמשקפיים");
        return;
    }

    const name = reverse_if_hebrew(document.getElementById("name").value);
    const surname = reverse_if_hebrew(document.getElementById("surname").value);
    var enc = new TextEncoder('utf-8'); 

    if (name == null || surname == null) { // Check that they are filled.
        alert("יש למלא את השדות שם פרטי ושם משפחה");
        return;
    }

    service = await SERVER.getPrimaryService('4f464e49-0000-1000-8000-00805f9b34fb') // UUID of id service
    characteristic = await service.getCharacteristic('46495253-5430-4e41-4d45-303030303030') // UUID of name
    console.log("Uploading name")
    console.log(name)
    await characteristic.writeValue(enc.encode(name))
    console.log("Uploaded name")
    characteristic = await service.getCharacteristic('4c415354-304e-414d-4530-303030303030') // UUID of surname
    console.log("Uploading surname")
    console.log(surname)
    await characteristic.writeValue(enc.encode(surname))
    console.log("Uploaded surname")
    console.log("Upload succeeded")
    alert("כתיבה בוצעה בהצלחה");
}

// This function reads the device using the JSON in the top
// It returns the data formatted the same as the JSON
async function read_all(server) {
    let res = {};
    let services = await server.getPrimaryServices();
    for(const service of services) {
        let service_json = services_json[service.uuid];
        let characteristic_values = {};
        let characteristics = await service.getCharacteristics();
        for( const characteristic of characteristics ) {
            let characteristic_json = service_json['characteristics'][characteristic.uuid];
            let characteristic_value = await characteristic.readValue();
            let decoder = new TextDecoder('utf-8');
            let value_str = decoder.decode(characteristic_value);

            // This is a special handle for friends
            if ( value_str[0] == '[' && value_str[value_str.length-1] == ']' ) {
                let value_tmp = value_str.substring(1, value_str.length-1)
                let value_array = value_tmp.split(',');
                value_array = value_array.map(val => {return reverse_if_hebrew(val)} );
                // for(const val of value_array) { reverse_if_hebrew(val); }
                characteristic_json['value'] = value_array;
                characteristic_values[characteristic.uuid] = characteristic_json;
                continue;
            }

            value_str = reverse_if_hebrew(value_str);
            characteristic_json['value'] = value_str;
            characteristic_values[characteristic.uuid] = characteristic_json;

        }
        service_json['characteristics'] = characteristic_values;
        res[service.uuid] = service_json;
    };
    var promise = new Promise(resolve => resolve(res) );
    return promise;
}


// This function recieves a table and array, and populate the array into the table
function populate_table(arr, table, start=1, splitter='-', ignore_first=1) {
    // TODO: Move the hebrew reverse to here
    for( i=0; i<arr.length; i++ ) {
        let row = table.insertRow(i+start); // The titles are line 0
        if (splitter != null) {
            let vals = arr[i].split(splitter);
            for (j=0; j<vals.length-1; j++) {
                row.insertCell(j).innerHTML = vals[j+ignore_first];
            }
            continue;
        }

        row.insertCell(0).innerHTML = arr[i]; // If no splitter is given, just put it in the first cell.
    }

}

// This function reverses the characters if they contain Hebrew
function reverse_if_hebrew(val) {
    // TODO: Handle complex hebrew + english situations. e.g: "477b404aea03-אלמוני-פלוני"
    if ( val.search(/[\u0590-\u05FF]/) >= 0 ) { // Regex for hebrew
        val = val.split("").reverse().join("");
    }
    return val;
}


// This function auto load with page
//And checks if it was reloaded by the JS itself, run first_scan()
window.onload = function() { 
    var reloading = sessionStorage.getItem("reloading");
    if (reloading) {
        sessionStorage.removeItem("reloading");
        // Handle the manual refresh somehow.
        // If I'm trying to run `first_scan()` I get: `DOMException: Must be handling a user gesture to show a permission request.`
        // first_scan();
    }
}